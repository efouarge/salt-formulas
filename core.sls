include:
  - vim
  - ntp
  - ssh
  - epel
  - hosts
  - nagios
  - diamond
