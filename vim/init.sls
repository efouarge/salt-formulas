# Install and Configure Vim

vim:
 pkg:
  - installed
  - name: vim-enhanced


/etc/vimrc
 file:
   - managed
   - source: salt://vim/vimrc
   - user: root
   - group: root
   - mode: 644
   - template: jinja
   - makedirs: True
   - require 
     - pkg: vim
