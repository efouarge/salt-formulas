include:
  - ssh

sshd:
  service:
    - running
  require:
    - pkg: openssh
    - file: /etc/ssh/banner
    - file: /etc/ssh/sshd_config

/etc/ssh/sshd_config
  file:
    - managed
    - user: root
    - group: root
    - mode: 644
    - source: salt://ssh/sshd_config
    - require:
        - pkg: openssh

/etc/ssh/banner
  file:
    - managed
    - user: root
    - group: root
    - source: salt://ssh/banner
    - require:
        - pkg: openssh-server
